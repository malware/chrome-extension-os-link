linkApp.controller('MainCtrl', ['$scope', 'LinkService', 'SelectionService', function($scope, LinkSrv, SelectSrv) {
	var link = $scope.link = {};
	link.source = "";
	link.isOk = true;

	$scope.select = function(elementID) {
		SelectSrv.selectText(elementID);
	};

	$scope.$watch('link.source', function(value) {
		if (!!value) {
			LinkSrv.init(value);

			link.mac = LinkSrv.formatMac();
			link.win = LinkSrv.formatWin();
			link.isOk = LinkSrv.isLinkValid();
		}
	});
}]);