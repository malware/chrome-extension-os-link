linkApp.service('SelectionService', function() {
	return {
		selectText: function(elementID) {
			var doc = document;
			var text = doc.getElementById(elementID);
			var range;

			if (doc.body.createTextRange) { // ms
				range = doc.body.createTextRange();
				range.moveToElementText(text);
				range.select();
			} else if (window.getSelection) { // moz, opera, webkit
				var selection = window.getSelection();
				range = doc.createRange();
				range.selectNodeContents(text);
				selection.removeAllRanges();
				selection.addRange(range);
			}
		}
	};
});