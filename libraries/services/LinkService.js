linkApp.service('LinkService', function() {
	this.parsedLink = "";
	return {
		init: function(link) {
			var workLink = link;
			// trim
			workLink = workLink.replace(/^\s+|\s+$/gm, '');
			// prefix to : (http:, smb:, ...)
			workLink = workLink.replace(/^(\w+\:)?/im, '');
			// slash replace
			workLink = workLink.replace(/[\\/]/g, '|');

			this.parsedLink = workLink;
		},

		formatMac: function() {
			return 'smb:' + this.parsedLink.replace(/[\|]/g, '/');
		},

		formatWin: function() {
			return this.parsedLink.replace(/[\|]/g, '\\');
		},

		isLinkValid: function() {
			return this.parsedLink ? this.parsedLink.match(/^[a-zA-Z0-9\-\._\|]+$/g) : false;
		}
	};
});